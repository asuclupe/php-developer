<?php
// Routes
$app->get('/', function ($request, $response, $args) {
    $data = file_get_contents('../data/employees.json');
    $employees = json_decode($data, true);
    return $this->renderer->render($response, 'index.phtml', ['employees' => $employees]);
});

$app->post('/', function($request, $response, $args){
    $email = $request->getParam('email');
    $data = file_get_contents('../data/employees.json');
    $employees = json_decode($data, true);
    if(empty($email) || $email === '')
      return json_encode($employees);

    $filter = array_filter($employees, function($data) use ($email){
        $exist = like_match('%'.$email.'%', $data['email']);
        if( $exist )
          return $data;
    });
    return json_encode($filter);
    //return $this->renderer->render($response, 'index.phtml', ['employees' => $filter]);
});

function like_match($pattern, $subject)
{
    $pattern = str_replace('%', '.*', preg_quote($pattern, '/'));
    return (bool) preg_match("/^{$pattern}$/i", $subject);
}

$app->post('/detail/employees', function($request, $response, $args){
    $code = $request->getParam('code');
    $data = file_get_contents('../data/employees.json');
    $employees = json_decode($data, true);
    $filter = array_filter($employees, function( $data ) use ( $code ){
        return $data['id'] == $code;
    });
    return json_encode(array_values($filter)[0]);
});

$app->get('/search/employee/{min_salary}/{max_salary}', function($request, $response, $args) use($app){
    $min_salary = (float) str_replace(array('$', ','), '', $args['min_salary']);
    $max_salary = (float) str_replace(array('$', ','), '', $args['max_salary']);
    $data = file_get_contents('../data/employees.json');
    $employees = json_decode($data, true);

    $filter = array_filter($employees, function($data) use ($min_salary, $max_salary){
        $salary = (float) str_replace(array('$', ','), '', $data['salary']);
        if( $salary >= $min_salary && $salary <= $max_salary)
          return $data;
    });

    $response = $this->renderer->render( $response, 'employees.xml', ['employees' => $filter]);
    $response = $response->withHeader('Content-type', 'application/xml');
    return $response;

});
