<?php
	class CompleteRange{
		public function build($array = [])
		{
			$new_array = [];
			$ini = $array[0];
			$fin = array_pop($array);  //  or  $array[ count($array) - 1 ];
			for ($i = $ini; $i <= $fin; $i++) {
				array_push($new_array, $i);
			}
			return $new_array;
		}
	}

	$obj = new CompleteRange();
    echo '<pre>';
    print_r( $obj->build([1,2,4,5]) );
    print_r( $obj->build([2,4,9]) );
    print_r( $obj->build([55,58,60]) );
    echo '</pre>';
