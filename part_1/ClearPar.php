<?php
	class ClearPar{
		public function build($value)
		{
			$result = '';
			for ($i = 0; $i < strlen($value); $i++) {
				if( $value{$i} === '(' ){
					$pos = $i + 1;
					$pareja = $this->buscaPareja($value, $pos);
					if($pareja === ')'){
						$result .= $value{$i}.$pareja;
						$i++;
					}
				}
			}
			return $result;
		}

		protected function buscaPareja($value, $pos){
			return isset($value{$pos}) ? $value{$pos} : '';
		}
	}

	$obj = new ClearPar();
	echo $obj->build('()())()');
	echo '<br>';
	echo $obj->build('()(()');
	echo '<br>';
	echo $obj->build(')(');
	echo '<br>';
	echo $obj->build('((()');
