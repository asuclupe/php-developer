<?php
	class ChangeString{

		public function build( $value )
		{
			for ($i=0; $i < strlen($value); $i++) {
				if( (ord($value{$i}) >= 97 && ord($value{$i}) < 122) ||
					(ord($value{$i}) >= 65 && ord($value{$i}) < 90) ){
					echo chr( ord($value{$i}) + 1 );
				}else if( ord($value{$i}) === 122 ){
					echo chr(97);
				}else if( ord($value{$i}) === 90 ){
					echo chr(65);
				}else{
					echo $value{$i};
				}
			}
		}

	}

	$obj = new ChangeString();

	$obj->build('123 abcd*3');
	echo '<br>';
	$obj->build('**Casa 52');
	echo '<br>';
	$obj->build('**Casa 52Z');
