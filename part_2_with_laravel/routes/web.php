<?php

Route::get('/', 'EmployeController@index');
Route::post('/search-by-email', 'EmployeController@searchByEmail')->name('search-by-email');
Route::post('/detail', 'EmployeController@detail')->name('detail');
