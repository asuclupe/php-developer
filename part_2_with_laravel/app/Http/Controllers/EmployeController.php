<?php

namespace App\Http\Controllers;

class EmployeController extends Controller
{
    protected $employees;

    public function __construct()
    {
        $data = file_get_contents(storage_path().'/data/employees.json');
        $this->employees = json_decode($data, true);
    }

    public function index(){
        return view('welcome', ['employees' => $this->employees]);
    }

    public function searchByEmail(){
        $email = request()->email;
        if(empty($email) || $email === '')
            return json_encode($this->employees);

        $filter = array_filter($this->employees, function($data) use ($email){
            $exist = $this->like_match('%'.$email.'%', $data['email']);
            if( $exist )
                return $data;
        });
        return json_encode($filter);
    }

    public function detail(){
        $code = request()->code;
        $filter = array_filter($this->employees, function( $data ) use ( $code ){
            return $data['id'] == $code;
        });
        return json_encode(array_values($filter)[0]);
    }

    private function like_match($pattern, $subject)
    {
        $pattern = str_replace('%', '.*', preg_quote($pattern, '/'));
        return (bool) preg_match("/^{$pattern}$/i", $subject);
    }

    public function searchBySalary($min, $max){
        $min_salary = (float) str_replace(array('$', ','), '', $min);
        $max_salary = (float) str_replace(array('$', ','), '', $max);

        $filter = array_filter($this->employees, function($data) use ($min_salary, $max_salary){
            $salary = (float) str_replace(array('$', ','), '', $data['salary']);
            if( $salary >= $min_salary && $salary <= $max_salary)
                return $data;
        });

        return response()->xml($filter, 200);
    }
}
