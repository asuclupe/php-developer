<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>List Employees</title>
    <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">
</head>
<body>
<div class="container">
    <h1 class="text-center">List Employees</h1>
    <!--form class="form-inline text-right" action="" method="post"-->
    <div class="row">
        <div class="col-md-10 col-sm-10">
            <input type="text" class="form-control" name="email" placeholder="Buscar por Email">
        </div>
        <div class="col-md-2 col-sm-2">
            <button id="btn-search" type="button" class="btn btn-primary btn-block"><span class="glyphicon glyphicon-search"></span> Buscar</button>
        </div>
    </div>
    <!--/form-->
    <br>
    <table class="table table-bordered table-hover">
        <thead>
        <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Position</th>
            <th>salary</th>
            <th>Opción</th>
        </tr>
        </thead>
        <tbody>
        @if(isset($employees))
            @foreach ($employees as $employee)
                <tr>
                    <td>{{$employee['name']}}</td>
                    <td>{{$employee['email']}}</td>
                    <td>{{$employee['position']}}</td>
                    <td class="text-right">{{$employee['salary']}}</td>
                    <td>
                        <button class="btn btn-xs btn-info btn-view-detail" data-value="{{$employee['id']}}">
                            <span class="glyphicon glyphicon-eye-open"></span>
                        </button>
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>

<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Detail Employee</h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="{{ asset('jQuery/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $.ajaxSetup({
            headers : {
                'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
            },
        });

        $('input[name="email"]').keyup(function(e){
            search( $(this).val() );
        })

        $('#btn-search').click(function(){
            search( $('input[name="email"]').val() );
        })

        $(document).on('click', '.btn-view-detail', function(){
            $.ajax({
                url : '{{route('detail')}}',
                type: 'POST',
                data: { 'code' : $(this).attr('data-value') },
                success : function(response) {
                    var _data = $.parseJSON(response);
                    var _html = '<p><label>Nombre:</label> '+_data.name+'</p>';
                    _html += '<p><label>Email:</label> '+_data.email+'</p>';
                    _html += '<p><label>Teléfono:</label> '+_data.phone+'</p>';
                    _html += '<p><label>Dirección:</label> '+_data.address+'</p>';
                    _html += '<p><label>Posición:</label> '+_data.position+'</p>';
                    _html += '<p><label>Salario:</label> '+_data.salary+'</p>';
                    _list = '<ol>';
                    $.each( _data.skills, function(item, value){
                        _list += '<li>'+value.skill+'</li>';
                    } )
                    _list += '</ol>';
                    _html += '<p><label>Skills:</label></p>'+_list;
                    $('.modal-body').html(_html);
                    $('#myModal').modal('show');
                }
            })
            return false;
        })
    })

    function search(value) {
        $.ajax({
            url: '{{route('search-by-email')}}',
            type: 'POST',
            data: { 'email' : value },
            success: function(response){
                var _data = $.parseJSON(response);
                var _tbody = '';
                $.each( _data, function(item, value){
                    _tbody += '<tr>';
                    _tbody += '<td>'+value.name+'</td>';
                    _tbody += '<td>'+value.email+'</td>';
                    _tbody += '<td>'+value.position+'</td>';
                    _tbody += '<td class="text-right">'+value.salary+'</td>';
                    _tbody += '<td><button class="btn btn-xs btn-info btn-view-detail" data-value="'+value.id+'"><span class="glyphicon glyphicon-eye-open"></span></button></td>';
                    _tbody += '</tr>';
                } )
                $('tbody').html(_tbody);
            }
        })
    }
</script>
</body>
</html>